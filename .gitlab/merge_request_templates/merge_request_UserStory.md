While sumiting this merge request I can ensure the following:

* [ ] All acceptance criteria(AC) from the user story are met
* [ ] A good grammar is used
* [ ] Documentation is updated

## Code quality

* [ ] Code meet general coding standard
* [ ] The code should respect the criteria of the [Style Guide](https://gitlab.com/AGEEI-CdP/SMS/wikis/style-guide)
* [ ] Project build without errors or warnings
* [ ] Tests written and passing
* [ ] 90% of code coverage

# Reviewer checklist:

## Code review

* [ ] Refactoring completed
* [ ] Peer Code Review performed
* [ ] Feature approved by the project owners
