All permanent member of the team are able to ensure the following:

* [ ] All User Stories included in the release are close and meet the **US DoD**
* [ ] All the acceptance criteria are met
* [ ] All issues are resolved
* [ ] All "TO DO" annotations are resolved
* [ ] All tests are green locally and on build server
* [ ] No Critical or Blocker bug exists
* [ ] No unintegrated work in progress has been left in any development or staging environment
* [ ] Ok from the team
