# SMS Interactive app
* __User story originale:__
"En tant qu'utilisateur d'application SMS  
Je veux une application SMS qui offre une expérience de communication plus diversifiée  
afin d'avoir une conversation émotionnellement plus riche et plus vraie."
* __Objectif principal:__
Afin d'aider à satisfaire notre besoin de communiquer et d'avoir des interactions sociales face à face, cette application vise à combler le grand écart entre une conversation chaleureuse avec un ami dans votre salon et une froide conversation SMS avec ce même ami à l'aide de fonctionnalités plus riche et juste que des simples emojis.
* __À propos des auteurs:__
Nous faisons partie d'un comité étudiant qui réalise des projets comme celui-ci afin de permettre aux étudiants de montrer leurs compétences, coder pour le plaisir, d'accroître leurs connaissances et repousser leurs limites. Voir la page du comité [ici](https://ageei.uqam.ca/).
* __Technologies:__
L'application est bâtie avec le cadre React-Native afin de supporter les appareils mobiles iOS et Android.  
Les fonctionnalités d'IA sont basées sur la librairie TensorFlow.
* __Work methodology:__
Nous utilisons notre propre version adaptée du développement piloté par les tests (TDD) et du développement axé sur le comportement (BDD) en vue de maintenir la qualité, la simplicité de la maintenance et l'évolutivité de l'application.  
Nous développons dans un environnement agile et publions des mises à jour mensuellement ou plus.  

Nous visons à produire un projet de la plus haute qualité et en sommes fiers.
## Documentation
La documentation pour les développeurs est disponible sur le [wiki](https://gitlab.com/AGEEI-CdP/SMS/wikis/home).
## Contribuer
Les membres de l'AGEEI sont automatiquement acceptés comme contribuant. Veuillez contacter l'association pour plus d'informations.  
Les contribuant extérieurs doivent faire une _request access_ et nous traiterons celles-ci individuellement.
## Licence
Cette application est sous la [licence MIT](./LICENCE.txt).
