# SMS Interactive app
* __Original user story:__
"As an SMS app user  
I want an SMS app that provides a more diversified communication experience  
so I can have a fulfilling and emotionally accurate conversation."
* __Main goal:__
In order to help fulfill our need to communicate and have social interaction face
to face, this app aims to fill the large and deep gap between a warm conversation
with a friend in your living room and the cold SMS interaction with that same
friend with functionalities more accurate and richer than simple emojis.
* __About the authors:__
We are part of a student committee that builds projects like this one to allow
students to show off their skills, code for fun, increase their knowledge and
push their limits.
See the committee page [here](https://ageei.uqam.ca/).
*  __Technologies:__
The app is built with the React-native framework in order to support Android and iOS devices.  
The AI functionalities are based on the TensorFLow library.
* __Work methodology:__
We use our own adapted version of Test Driven Development and Behavior Driven
Development in order to maintain quality, simple maintenance and scalability of
the app.
We develop in an agile environment and release updates monthly or more.

We aim for the highest quality of work and are proud of it.

## Documentation
Developer's documentation is available in the [wiki](https://gitlab.com/AGEEI-CdP/SMS/wikis/home).
## Contributing
Members of AGEEI can automatically become contributors. Please contact the student association for more information.  
External contributors have to make a request access and we will treat each one individually.
## License
This application is [MIT Licensed](./LICENSE.txt).
